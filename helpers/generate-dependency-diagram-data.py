#!/usr/bin/python3
import os
import sys
import argparse
import subprocess
from helperslib import BuildSpecs, BuildSystem, CommonUtils, EnvironmentHandler, Packages

# Parse the command line arguments we've been given
parser = argparse.ArgumentParser(description='Utility to generate dependency diagram information for use by api.kde.org.')
parser.add_argument('--product', type=str, required=True)
parser.add_argument('--project', type=str, required=True)
parser.add_argument('--branchGroup', type=str, required=True)
parser.add_argument('--platform', type=str, required=True)
parser.add_argument('--usingInstall', type=str, required=True)
arguments = parser.parse_args()

# Determine the environment we need to provide for the compilation process
buildEnvironment = EnvironmentHandler.generateFor( installPrefix=arguments.usingInstall )

# Determine where our source code is checked out to and where we will be building it
# We'll assume that the directory we're running from is where the sources are located
sourcesLocation = os.getcwd()

# Are we allowed to run?
# We only gather this metadata from the principal Linux platform, which at the moment is SUSEQt5.12 for everything
if arguments.platform != 'SUSEQt5.12':
	# Then there is nothing for us to do
	sys.exit(0)

# First determine where we the data will be stored, both temporarily and on the server
# As the API documentation can only be generated once, and we have the greatest capacity available for Linux we will use Linux dependency diagrams on api.kde.org.
outputDirectory = os.path.join( sourcesLocation, 'dotdata' )
remoteStoragePath = os.path.join('/home/api/depdiagram-output/', Packages.nameForProject(arguments.product, arguments.project, arguments.branchGroup))

# Build up the command to run
commandToRun = 'python "{0}/kapidox/src/depdiagram-prepare" -s "{1}" "{2}"'
commandToRun = commandToRun.format( CommonUtils.scriptsBaseDirectory(), sourcesLocation, outputDirectory )

# Run the command, which will generate a pile of *.dot files for us
process = subprocess.Popen( commandToRun, stdout=sys.stdout, stderr=sys.stderr, shell=True, env=buildEnvironment )
process.wait()

# Do we have something to upload?
if not os.path.exists(outputDirectory):
	# Then exit gracefully, nothing for us to do!
	sys.exit(0)

# Connect to the server to upload the files
privateKeyFile = os.path.join( os.path.expanduser('~'), 'Keys', 'api-access.key')
client = CommonUtils.establishSSHConnection( 'zivo.kde.org', 'api', privateKeyFile )
# Bring up a SFTP session
sftp = client.open_sftp()

# Does our storage path exist?
if not CommonUtils.sftpFileExists(sftp, remoteStoragePath):
	# Create it then!
	sftp.mkdir(remoteStoragePath)

# Make sure it has been cleaned out of anything which is in there
# This is necessary to ensure any dependency or component which has been dropped doesn't hang around unnecessarily
fileListing = sftp.listdir(remoteStoragePath)
for fileToRemove in fileListing:
	pathToRemove = os.path.join(remoteStoragePath, fileToRemove)
	sftp.remove(pathToRemove)

# Upload the files we've just generated
for fileToUpload in os.listdir(outputDirectory):
	# Determine the full local and remote paths
	fullLocalPath  = os.path.join(outputDirectory, fileToUpload)
	fullRemotePath = os.path.join(remoteStoragePath, fileToUpload)
	# Upload it!
	sftp.put(fullLocalPath, fullRemotePath)

# All done now, close the remote server connection
sftp.close()
client.close()

# And bow out gracefully
sys.exit(0)
